`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:27:34 05/23/2018
// Design Name:   binaryToASCIIHex
// Module Name:   C:/Users/George/Documents/work/EPCPOWER/Uartchallenge/bintoascii_tb.v
// Project Name:  Uartchallenge
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: binaryToASCIIHex
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module bintoascii_tb;

	// Inputs
	reg [15:0] binaryinput;

	// Outputs
	wire [31:0] ASCIIHexOutput;

	// Instantiate the Unit Under Test (UUT)
	binaryToASCIIHex uut (
		.binaryinput(binaryinput), 
		.ASCIIHexOutput(ASCIIHexOutput)
	);

	initial begin
		// Initialize Inputs
		binaryinput = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		binaryinput = 16'hAAAA;
		#5;
		binaryinput = 16'hA9A9;
		#5;
		binaryinput = 16'h0808;
		#5;
		binaryinput = 16'hF462;
		#5;
		binaryinput = 16'hDDDD;
		#5;
		binaryinput = 16'h0000;
		#5;
		
	end
      
endmodule

