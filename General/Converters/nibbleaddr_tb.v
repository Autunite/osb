`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:17:32 05/23/2018
// Design Name:   nibbleAddr
// Module Name:   C:/Users/George/Documents/work/EPCPOWER/Uartchallenge/nibbleaddr_tb.v
// Project Name:  Uartchallenge
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: nibbleAddr
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module nibbleaddr_tb;

	// Inputs
	reg [3:0] binaryIn;

	// Outputs
	wire [7:0] nibbleASCIIOut;

	// Instantiate the Unit Under Test (UUT)
	nibbleAddr uut (
		.binaryIn(binaryIn), 
		.nibbleASCIIOut(nibbleASCIIOut)
	);

	initial begin
		// Initialize Inputs
		binaryIn = 0;

		// Wait 100 ns for global reset to finish
		#100;
       
		// Add stimulus here
		binaryIn = 0;
		#5;
		
		binaryIn = 5;
		#5;
		binaryIn = 9;
		#5;
		binaryIn = 10;
		#5;
		binaryIn = 11;
		#5;
		
		binaryIn = 15;
		#5;
		
		binaryIn = 13;
		#5;
	end
      
endmodule

