`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   00:31:02 08/24/2017
// Design Name:   clockdiv
// Module Name:   C:/Users/Joi/Desktop/Work/conways/multiplexorROM/displaymultiplex/clockdiv_tb.v
// Project Name:  displaymultiplex
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: clockdiv
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module clockdiv_tb;

	// Inputs
	reg clk;
	reg reset;
	// Outputs
	wire divClk;

	// Instantiate the Unit Under Test (UUT)
	clockdiv uut (
		.clk(clk), 
		.reset(reset),
		.divClk(divClk)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 0;
		// Wait 100 ns for global reset to finish
		#100;
        reset = 1;
		  #40;
		  reset=0;
		// Add stimulus here

	end
      always
	#10 clk =  ~clk;
endmodule

