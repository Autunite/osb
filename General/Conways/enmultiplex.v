`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:30:34 08/22/2017 
// Design Name: 
// Module Name:    enmultiplex 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module enmultiplex(
    input [2:0] addr,
    input en,
    output reg [7:0] row
    );
always@(en, addr)
begin
	if(en==1)
	begin
		case(addr)
		0: row<=8'b00000001;
		1: row<=8'b00000010;
		2: row<=8'b00000100;
		3: row<=8'b00001000;
		4: row<=8'b00010000;
		5: row<=8'b00100000;
		6: row<=8'b01000000;
		7: row<=8'b10000000;
		endcase
	end
	else
	begin
		row <=8'b00000000;
	end
end

endmodule
