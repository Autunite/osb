`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:29:48 09/12/2017
// Design Name:   fourByFourCell
// Module Name:   C:/Users/George/Documents/College/SDSU/COMPE70L/conways/multiplexorROM/displaymultiplex/fbf_tb.v
// Project Name:  displaymultiplex
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: fourByFourCell
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module fbf_tb;

	// Inputs
	reg [43:0] neighbors;
	reg clk;
	reg load;
	reg [3:0] in0;
	reg [3:0] in1;
	reg [3:0] in2;
	reg [3:0] in3;

	// Outputs
	wire [3:0] out0;
	wire [3:0] out1;
	wire [3:0] out2;
	wire [3:0] out3;

	// Instantiate the Unit Under Test (UUT)
	fourByFourCell uut (
		.neighbors(neighbors), 
		.clk(clk), 
		.load(load), 
		.in0(in0), 
		.in1(in1), 
		.in2(in2), 
		.in3(in3), 
		.out0(out0), 
		.out1(out1), 
		.out2(out2), 
		.out3(out3)
	);

	initial begin
		// Initialize Inputs
		neighbors = 0;
		clk = 0;
		load = 0;
		in0 = 0;
		in1 = 0;
		in2 = 0;
		in3 = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		//load pulser in upper left
		neighbors = 0;
		clk = 0;
		load = 0;
		in0 = 4'b0100;
		in1 = 4'b0100;
		in2 = 4'b0100;
		in3 = 4'b0000;
			//set load = 1
			#5;
			load = 1;
			#5;
			//toggle clock
			clk = 1;
			#5;
			clk = 0;
			#5;
			//set load = 0
			load = 0;
			#5;
			//toggle clock 3 times
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			
		//load pulser in upper right
		neighbors = 0;
		clk = 0;
		load = 0;
		in0 = 4'b0010;
		in1 = 4'b0010;
		in2 = 4'b0010;
		in3 = 4'b0000;
			//set load = 1
			#5;
			load = 1;
			#5;
			//toggle clock
			clk = 1;
			#5;
			clk = 0;
			#5;
			//set load = 0
			load = 0;
			#5;
			//toggle clock 3 times
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			
		//load pulser in bottom left
		neighbors = 0;
		clk = 0;
		load = 0;
		in0 = 4'b0000;
		in1 = 4'b0100;
		in2 = 4'b0100;
		in3 = 4'b0100;
			//set load = 1
			#5;
			load = 1;
			#5;
			//toggle clock
			clk = 1;
			#5;
			clk = 0;
			#5;
			//set load = 0
			load = 0;
			#5;
			//toggle clock 3 times
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			
		//load pulser in bottom right
		neighbors = 0;
		clk = 0;
		load = 0;
		in0 = 4'b0000;
		in1 = 4'b0010;
		in2 = 4'b0010;
		in3 = 4'b0010;
			//set load = 1
			#5;
			load = 1;
			#5;
			//toggle clock
			clk = 1;
			#5;
			clk = 0;
			#5;
			//set load = 0
			load = 0;
			#5;
			//toggle clock 3 times
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			
			//now test the neighbors to get the four corners to light up
			
			//load 0
			neighbors = 0;
			clk = 0;
			load = 0;
			in0 = 4'b0000;
			in1 = 4'b0000;
			in2 = 4'b0000;
			in3 = 4'b0000;
			//set load = 1
			#5;
			load = 1;
			#5;
			//toggle clock
			clk = 1;
			#5;
			clk = 0;
			#5;
			//set load = 0
			load = 0;
			#5;
			
			neighbors = 44'b10100001011101000010111010000101110100001011;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			
			//load in 3 cell blocks that turn into 2x2 stable blocks to test diagonal connections
			
			//top middle
			neighbors = 0;
		clk = 0;
		load = 0;
		in0 = 4'b0010;
		in1 = 4'b0110;
		in2 = 4'b0000;
		in3 = 4'b0000;
			//set load = 1
			#5;
			load = 1;
			#5;
			//toggle clock
			clk = 1;
			#5;
			clk = 0;
			#5;
			//set load = 0
			load = 0;
			#5;
			//toggle clock 3 times
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			
			//left middle
			neighbors = 0;
		clk = 0;
		load = 0;
		in0 = 4'b0000;
		in1 = 4'b0100;
		in2 = 4'b1100;
		in3 = 4'b0000;
			//set load = 1
			#5;
			load = 1;
			#5;
			//toggle clock
			clk = 1;
			#5;
			clk = 0;
			#5;
			//set load = 0
			load = 0;
			#5;
			//toggle clock 3 times
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			
			//middle middle
			neighbors = 0;
		clk = 0;
		load = 0;
		in0 = 4'b0000;
		in1 = 4'b0010;
		in2 = 4'b0110;
		in3 = 4'b0000;
			//set load = 1
			#5;
			load = 1;
			#5;
			//toggle clock
			clk = 1;
			#5;
			clk = 0;
			#5;
			//set load = 0
			load = 0;
			#5;
			//toggle clock 3 times
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			
			//right middle
			neighbors = 0;
		clk = 0;
		load = 0;
		in0 = 4'b0000;
		in1 = 4'b0001;
		in2 = 4'b0011;
		in3 = 4'b0000;
			//set load = 1
			#5;
			load = 1;
			#5;
			//toggle clock
			clk = 1;
			#5;
			clk = 0;
			#5;
			//set load = 0
			load = 0;
			#5;
			//toggle clock 3 times
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			
			//bottom middle
			neighbors = 0;
		clk = 0;
		load = 0;
		in0 = 4'b0000;
		in1 = 4'b0000;
		in2 = 4'b0010;
		in3 = 4'b0110;
			//set load = 1
			#5;
			load = 1;
			#5;
			//toggle clock
			clk = 1;
			#5;
			clk = 0;
			#5;
			//set load = 0
			load = 0;
			#5;
			//toggle clock 3 times
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			clk = 1;
			#5;
			clk = 0;
			#5;
			
	end
      
endmodule

