`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:32:30 08/23/2017
// Design Name:   FSM
// Module Name:   C:/Users/Joi/Desktop/Work/conways/multiplexorROM/displaymultiplex/fsm_tb.v
// Project Name:  displaymultiplex
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: FSM
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module fsm_tb;

	// Inputs
	reg load;
	reg clk;

	// Outputs
	wire [2:0] cAddr;
	wire [2:0] rAddr;
	wire en;
	wire LDO;

	// Instantiate the Unit Under Test (UUT)
	FSM uut (
		.load(load), 
		.clk(clk), 
		.cAddr(cAddr), 
		.rAddr(rAddr), 
		.en(en), 
		.LDO(LDO)
	);

	initial begin
		// Initialize Inputs
		load = 0;
		clk = 0;

		// Wait 100 ns for global reset to finish
		#100;
      load = 1;
		#20;
		load = 0;
		#20
		
		#1000;
		load = 1;
		#20;
		load = 0;
		#20;
		// Add stimulus here

	end
      
	always
	#10 clk =  ~clk;
	
		
endmodule

