`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:28:04 08/22/2017 
// Design Name: 
// Module Name:    multiplexor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module multiplexor(
    input [7:0] row0,
    input [7:0] row1,
    input [7:0] row2,
    input [7:0] row3,
    input [7:0] row4,
    input [7:0] row5,
    input [7:0] row6,
    input [7:0] row7,
    input load,
	 input clk,
    output  [7:0] column ,
    output  [7:0] row 
    );

wire [7:0] uInvert;
wire en;
wire [2:0] rAddr;
wire [2:0] cAddr;
wire ld0;

eightxeightmem U0(
    .row0(row0),
    .row1(row1),
    .row2(row2),
    .row3(row3),
    .row4(row4),
    .row5(row5),
    .row6(row6),
    .row7(row7),
    .Load(ld0),
    .addr(cAddr),
    .out(uInvert)
    );

FSM U1(
    .load(load),
    .clk(clk),
    .cAddr(cAddr),
    .rAddr(rAddr),
    .en(en),
    .LDO(ld0)
    );
	 
enmultiplex U2(
    .addr(rAddr),
    .en(en),
    .row(row)
    );



assign column = ~uInvert;

endmodule
