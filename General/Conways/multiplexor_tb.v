`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:56:17 08/23/2017
// Design Name:   multiplexor
// Module Name:   C:/Users/Joi/Desktop/Work/conways/multiplexorROM/displaymultiplex/multiplexor_tb.v
// Project Name:  displaymultiplex
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: multiplexor
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module multiplexor_tb;

	// Inputs
	reg [7:0] row0;
	reg [7:0] row1;
	reg [7:0] row2;
	reg [7:0] row3;
	reg [7:0] row4;
	reg [7:0] row5;
	reg [7:0] row6;
	reg [7:0] row7;
	reg load;
	reg clk;

	// Outputs
	wire [7:0] column;
	wire [7:0] row;

	// Instantiate the Unit Under Test (UUT)
	multiplexor uut (
		.row0(row0), 
		.row1(row1), 
		.row2(row2), 
		.row3(row3), 
		.row4(row4), 
		.row5(row5), 
		.row6(row6), 
		.row7(row7), 
		.load(load), 
		.clk(clk), 
		.column(column), 
		.row(row)
	);

	initial begin
		// Initialize Inputs
		row0 = 1;
		row1 = 2;
		row2 = 3;
		row3 = 4;
		row4 = 5;
		row5 = 6;
		row6 = 7;
		row7 = 8;
		load = 0;
		clk = 0;

		// Wait 100 ns for global reset to finish
		#100;
      load = 1;
		#40;
		load = 0;
		#40;
		row0 = 8;
		row1 = 7;
		row2 = 6;
		row3 = 5;
		row4 = 4;
		row5 = 3;
		row6 = 2;
		row7 = 1;
		
		#1000;
		load = 1;
		#40;
		load = 0;
		// Add stimulus here

	end

always
	#10 clk =  ~clk;    
	 
endmodule

