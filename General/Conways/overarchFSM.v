`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:22:38 09/19/2017 
// Design Name: 
// Module Name:    overarchFSM 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module overarchFSM(
    input loadi,
    input clk,
    output reg loadm =0,
    output reg loadcgl =0,
    output reg clocko =0
    );
reg [2:0] state = 2'b00;
reg [13:0] count = 0;

always@(posedge clk)
begin
	if(loadi ==1)
	begin
		loadcgl  <=1;
		loadm 	<=0;
		clocko	<=0;
		state 	<=0;
		count		<=0;
	end
	
	else
	begin
		case(state)
		
			0: begin
				loadcgl  <=1;
				loadm		<=0;
				clocko	<=1;
				state <= state+1;
			end
			
			1: begin
				loadcgl  <=0;
				loadm		<=1;
				clocko	<=0;
				state <= state+1;
			end
			
			2: begin
				state <= state+1;
			end
			
			3: begin
					if(count ==6250)
					begin
						state 	<= state+1;
						count  <= 0;
					end
				
					else
					begin
						loadm   <=0;
						count	  <=  count +1;
					end
				end
			
			4: begin
				clocko <= 1;
				state	 <= 1;
			end
		endcase
	end
end

endmodule
