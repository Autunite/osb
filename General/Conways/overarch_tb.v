`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:13:31 09/19/2017
// Design Name:   overarch
// Module Name:   C:/Users/George/Documents/College/SDSU/COMPE70L/conways/multiplexorROM/displaymultiplex/overarch_tb.v
// Project Name:  displaymultiplex
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: overarch
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module overarch_tb;

	// Inputs
	reg clk;
	reg reset;
	reg clkReset;
	reg [1:0] switches;

	// Outputs
	wire slowClk;
	wire [7:0] anode;
	wire [7:0] cathode;

	// Instantiate the Unit Under Test (UUT)
	overarch uut (
		.clk(clk), 
		.reset(reset), 
		.clkReset(clkReset), 
		.switches(switches), 
		.slowClk(slowClk), 
		.anode(anode), 
		.cathode(cathode)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 0;
		clkReset = 0;
		switches = 0;

		// Wait 100 ns for global reset to finish
		#100;
      clkReset=1;
		#20;
		clkReset = 0;
		// Add stimulus here

	end
      
		always
	#10 clk =  ~clk;
		
endmodule

