`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:20:25 09/18/2017 
// Design Name: 
// Module Name:    toroidalwrapper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module toroidalwrapper(
    input clk,
    input load,
    input [7:0] in0,
    input [7:0] in1,
    input [7:0] in2,
    input [7:0] in3,
    input [7:0] in4,
    input [7:0] in5,
    input [7:0] in6,
    input [7:0] in7,
    output [7:0] out0,
    output [7:0] out1,
    output [7:0] out2,
    output [7:0] out3,
    output [7:0] out4,
    output [7:0] out5,
    output [7:0] out6,
    output [7:0] out7
    );

wire [91:0] toroid;

eightByEightCell U1(
    .neighbors(toroid),
    .clk(clk),
	 .load(load),
    .in0(in0),
    .in1(in1),
    .in2(in2),
    .in3(in3),
    .in4(in4),
    .in5(in5),
    .in6(in6),
    .in7(in7),
    .out0(out0),
    .out1(out1),
    .out2(out2),
    .out3(out3),
    .out4(out4),
    .out5(out5),
    .out6(out6),
    .out7(out7)
    );
	 
	 //assigns the outputs of one edge of the 8x8 grid to the inputs on the opposite side of the grid
	 assign toroid = 
	 {
		//91
		out7[7],
		//90
		out7[6],
		//89
		out7[7],
		//88
		out7[6],
		//87
		out7[5],
		//86
		out7[6],
		//85
		out7[5],
		//84
		out7[4],
		//83
		out7[5],
		//82
		out7[4],
		//81
		out7[3],
		//80
		out7[4],
		//79
		out7[3],
		//78
		out7[2],
		//77
		out7[3],
		//76
		out7[2],
		//75
		out7[1],
		//74
		out7[2],
		//73
		out7[1],
		//72
		out7[0],
		//71
		out7[1],
		//70
		out7[0],
		//69 corner
		out7[7],
		//68
		out0[7],
		//67
		out1[7],
		//66
		out0[7],
		//65
		out1[7],
		//64
		out2[7],
		//63
		out1[7],
		//62
		out2[7],
		//61
		out3[7],
		//60
		out2[7],
		//59
		out3[7],
		//58
		out4[7],
		//57
		out3[7],
		//56
		out4[7],
		//55
		out5[7],
		//54
		out4[7],
		//53
		out5[7],
		//52
		out6[7],
		//51
		out5[7],
		//50
		out6[7],
		//49
		out7[7],
		//48
		out6[7],
		//47
		out7[7],
		//46 corner
		out0[7],
		//45
		out0[0],
		//44
		out0[1],
		//43
		out0[0],
		//42
		out0[1],
		//41
		out0[2],
		//40
		out0[1],
		//39
		out0[2],
		//38
		out0[3],
		//37
		out0[2],
		//36
		out0[3],
		//35
		out0[4],
		//34
		out0[3],
		//33
		out0[4],
		//32
		out0[5],
		//31
		out0[4],
		//30
		out0[5],
		//29
		out0[6],
		//28
		out0[5],
		//27
		out0[6],
		//26
		out0[7],
		//25
		out0[6],
		//24
		out0[7],
		//23 corner
		out0[0],
		//22
		out7[0],
		//21
		out6[0],
		//20
		out7[0],
		//19
		out6[0],
		//18
		out5[0],
		//17
		out6[0],
		//16
		out5[0],
		//15
		out4[0],
		//14
		out5[0],
		//13
		out4[0],
		//12
		out3[0],
		//11
		out4[0],
		//10
		out3[0],
		//9
		out2[0],
		//8
		out3[0],
		//7
		out2[0],
		//6
		out1[0],
		//5
		out2[0],
		//4
		out1[0],
		//3
		out0[0],
		//2
		out1[0],
		//1
		out0[0],
		//0 corner
		out7[0]
	 };

endmodule
