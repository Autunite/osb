`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   19:35:15 08/29/2017
// Design Name:   twoByTwoCell
// Module Name:   C:/Users/George/Documents/College/SDSU/COMPE70L/conways/multiplexorROM/displaymultiplex/twoxtwo_tb.v
// Project Name:  displaymultiplex
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: twoByTwoCell
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module twoxtwo_tb;

	// Inputs
	reg [19:0] neighbor;
	reg clk;
	reg load;
	reg [1:0] in0;
	reg [1:0] in1;

	// Outputs
	wire [1:0] out0;
	wire [1:0] out1;

	// Instantiate the Unit Under Test (UUT)
	twoByTwoCell uut (
		.neighbor(neighbor), 
		.clk(clk), 
		.load(load), 
		.in0(in0), 
		.in1(in1), 
		.out0(out0), 
		.out1(out1)
	);

	initial begin
		// Initialize Inputs
		neighbor = 0;
		clk = 0;
		load = 0;
		in0 = 0;
		in1 = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		in0 = 2'b11;
		in1 = 2'b01;
		#5;
		load = 1;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		load = 0;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		
		in0 = 2'b01;
		in1 = 2'b11;
		#5;
		load = 1;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		load = 0;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		
		in0 = 2'b10;
		in1 = 2'b11;
		#5;
		load = 1;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		load = 0;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		
		in0 = 2'b11;
		in1 = 2'b10;
		#5;
		load = 1;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		load = 0;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		
		in0 = 2'b01;
		in1 = 2'b01;
		#5;
		load = 1;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		load = 0;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		
		in0  = 2'b00;
		in1  = 2'b00;
		#5;
		load = 1;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		load = 0;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		neighbor = 20'b10101010111010101011; 
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
	end
      
endmodule

