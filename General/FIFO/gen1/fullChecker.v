`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:32:33 06/04/2018 
// Design Name: 
// Module Name:    fullChecker 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module fullChecker(
    input [5:0] rIndex,
    input [5:0] wIndex,
    output reg isFull
    );
	 
	//number to help calculate fullness. Ram array is 64 memory banks
	//use 128 to make wrap around errors less of a pain
	//reg [6:0] fillCalculationReg;
	 
	 initial begin
		isFull <=0;
	 end
	 
	
	
	wire signed [6:0] r_signed, w_signed, fillCalculationReg, indexedReg;
	
	assign r_signed = rIndex;
	assign w_signed = wIndex;
	assign fillCalculationReg = r_signed - w_signed;
	
	//assign isFull = (writeAddr+17 >=readAddr) ? 1:0;
	assign indexedReg = (fillCalculationReg<0) ? (fillCalculationReg+64) : fillCalculationReg;
	
	//assign isFull = (writeAddr+17 >=readAddr) ? 1:0;
	
	always@(indexedReg)
	begin
		
		if(indexedReg > 17)
		begin
			isFull <= 0;
		end
		
		else if(indexedReg <= 17 && indexedReg != 0)
		begin
			isFull <= 1;
		end
		
		//else read - write is equal to 0, assume buffer is empty
		else
		begin
			isFull <=0;
		end
		
	end


endmodule
