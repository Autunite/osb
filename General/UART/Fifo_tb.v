`timescale 1ns / 1ps

module Fifo_tb;


//inputs
	reg FIFO_IN[7:0];
	reg reset;
	reg latchIn;
	reg latchOut;
	reg clk;
	
	//outputs
    wire FIFO_OUT[7:0];
	wire isFull;
	wire isEmpty;
	
	fourxeight_FIFO (
	.depth(4), 
	.width(8),
	.addrSize(2)
	) 
	
	uut(
	//inputs
	.FIFO_IN(FIFO_IN),
	.reset(reset),
	.latchIn(latchIn),
	.latchOut(latchOut),
	.clk(clk),
	
	//outputs
    .FIFO_OUT(FIFO_OUT),
	.isFull(isFull),
	.isEmpty(isEmpty)
	
	);

	initial begin
	
	FIFO_IN = 0;
	reset = 0;
	latchIn = 0;
	latchOut = 0;
	clk = 0;
	
	#10;
	reset = 1;
	#10;
	reset = 0;
	#10;
	
	end
	
	always 
       #5  clk =  ! clk;
	
endmodule