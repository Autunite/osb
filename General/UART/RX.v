`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Bushnelllabs
// Engineer: George Bushnell
// 
// Create Date: 09/05/2018 01:18:39 PM
// Design Name: 
// Module Name: RX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: This module handles the Receiving side of the
//	UART
// 
//////////////////////////////////////////////////////////////////////////////////

module RX
(
	input RX,
	input clk,
	input latchIn,
	output [7:0] RX_REG
);

endmodule