`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/05/2018 01:18:39 PM
// Design Name: 
// Module Name: UARTWrapper
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module UARTWrapper(
    input RX,
    output TX,
    input Clk,
    output TX_Full,
    output TX_Empty,
    output RX_Full,
    output RX_Empty,
    input [7:0] TX_IN,
    output [7:0] RX_Out,
    input TX_LatchIn,
    input RX_LatchOut,
	input reset
    );
	
	RX R1(
	.RX(),
	.clk(),
	.latchIn(),
	.RX_REG()
	);
	
	TX T1(
	.TX_REG(),
	.clk(),
	.latchOut(),
	.TX()
	);
	
	fourxeight_FIFO TX_BUFF(
    .FIFO_IN(),
    .FIFO_OUT(),
    .clk(),
	.reset(reset),
    .isFull(),
    .isEmpty()
    );
	
	fourxeight_FIFO RX_BUFF(
    .FIFO_IN(),
    .FIFO_OUT(),
    .clk(),
    .isFull(),
    .isEmpty()
    );
	
endmodule
