`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date: 09/05/2018 01:18:39 PM
// Design Name: 
// Module Name: 4x8_FIFO
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// This is a general purpose nx8 FIFO that uses synchronous
//	latchin and latchout control signals.
//	It will first be used in the UART I am building for the
//	COMPE 470 class but will be built using parameters so that it
//	can be used in other projects
//////////////////////////////////////////////////////////////////////////////////

//if you change these values
//then update the addr size


module fourxeight_FIFO
	(
	//inputs
	FIFO_IN,
	reset,
	latchIn,
	latchOut,
	clk,
	
	//outputs
    FIFO_OUT,
	isFull,
	isEmpty
    );
	
	
	parameter depth = 4;
	parameter width = 8;
	parameter addrSize = 2;
	
	input [width-1:0] FIFO_IN;
    output [width-1:0] FIFO_OUT;
	input reset;
	input latchIn;
	input latchOut;
    input clk;
    output isFull;
    output isEmpty;
	
	//set these to be equal
	//to log2 of the depth
	//I can't be bothered
	//to figure out the work
	//around to do this
	reg [addrSize-1:0] wrAddr;
	reg [addrSize-1:0] reAddr;
	reg [addrSize-1:0] iterator = 0;
	
	//this is the fifo buffer
	reg [width-1:0] buffer [depth-1:0];
	
	//Figure out the full and empty flags
	//Circular buffer is used so figure out how to deal
	//with detecting whether the write pointer gets within
	//a critical range of the read pointer
	//Care must be taken to make sure that wrap around
	//logic of the circular buffer
	
	//assignment to update the isFull flag
	//doesn't work with edge cases
	//assign isFull = (writeAddr+17 >=readAddr) ? 1:0;
	
	//assignment to update the isEmpty flag
	assign isEmpty = (wrAddr==reAddr) ? 1:0;
	
	//assignment to update the isFull flag
	assign isFull = (wrAddr+1==reAddr) ? 1:0;
	
	//fifo out always has the output
	//this is done to minimize latency
	//from the clock to the UART
	assign FIFO_OUT = buffer[reAddr];
	
	//main part of the module
	//set to zero upon reset
	//when writing to the ram make sure that
	//latchIn and latchOut are 1 for only one clock cycle
	//putting in a one shot built in so that they are internal
	//to this module
	//
	always@(posedge clk)
	begin
		//reset memory to 0
		if(reset)
		begin
			reAddr 	<= 0;
			wrAddr 	<= 0;
			//FIFO_OUT 		= 8'h00;
			for(iterator = 0 ; iterator <(depth-1); iterator = iterator +1)
			begin
				buffer[iterator] <= 0;
			end
		end
		
		//on clockedge check case statements
		else casez({isFull, isEmpty, latchIn, latchOut})
		
			//
			default:
			begin
			
			end
			
			//isFull and latchin is asserted
			4'b1?1?:
			begin
			
			end
			
			//isEmpty and latchout is asserted
			4'b?1?1:
			begin
			
			end
			
			//isFull == 0 and Latch in is asserted
			4'b0?1?:
			begin
				buffer[wrAddr]	<= FIFO_IN;
				wrAddr			<= wrAddr + 1; 
			end
			
			//isEmpty == 0 and latch out is asserted
			//latchIn is not asserted
			4'b?001:
			begin
				//buffer[reAddr]	<= FIFO_IN;
				reAddr			<= reAddr + 1;
			end
			
			//latchout and latchin are asserted at the same time
			4'b?011:
			begin
				buffer[wrAddr]	<= FIFO_IN;
				wrAddr			<= wrAddr + 1;
				reAddr			<= reAddr + 1;				
			end
			
		endcase
		
		
	end
	
	
	
endmodule
