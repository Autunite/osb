`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Physics Professors and that one Embedded Dude
// Engineer: George Bushnell
// 
// Create Date:    19:38:02 07/25/2018 
// Design Name: 
// Module Name:    convoluter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module convoluter(
    input signed [15:0] currentInput,
    input clock,
	 input reset,
    output reg signed [15:0] voltageOutput
    );
	
	//ROM that contains the signed values
	//of the desired impulse response
	wire signed [15:0] impulse [511:0];
	
	//shift register that takes in the signal to be 
	//convoluted
	reg signed [15:0] inputShiftRegister[511:0];
	
	//result of the multiplication
	reg signed [15:0] product[511:0];
	
	//put the series resistance here
	reg signed [15:0] resistance = 1;
	
	//put negative inductance value here
	reg signed [15:0] inductance = -100;
	
	//int used for the loops
	integer looper;
	
	//generates the rom for the
	//impulse response
	//let the phys profs give me an equation
	//that I can mutilate into bitwise form
	genvar gi;
	generate
		for(gi = 0 ; gi <= 512; gi = gi +1)
		begin : Genvalue
			assign impulse[gi] = -resistance/inductance*2.7182818**(gi/(inductance/resistance));
		end
	endgenerate	
	
	always@(posedge clock)
	begin
		//on reset, set everything to zero
		if(reset == 1)
		begin
			for(looper = 0; looper <=511; looper = looper +1)
			begin
				product[looper] 				<= 0;
				voltageOutput					<= 0;
				inputShiftRegister[looper]	<= 0;
			end
		end
		
		else
		begin
			inputShiftRegister[0] <= currentInput;
			
			//multiplication loop
			for(looper = 0; looper <= 511; looper = looper +1)
			begin
				//product accumulates product of input signal and impulse
				product[looper] <= inputShiftRegister[looper] * impulse[looper];
			end
			
			//loop to handle the parallel shift
			for(looper = 0; looper<=510; looper = looper +1)
			begin
				inputShiftRegister[looper+1] <= inputShiftRegister[looper];
			end
			
			//summation loop
			for(looper = 0; looper <= 511; looper = looper +1)
			begin
				voltageOutput <= voltageOutput + product[looper];
			end
			
			
		end
		
	end

endmodule
