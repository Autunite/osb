`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:58:36 08/18/2017 
// Design Name: 
// Module Name:    SIPO1to8 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//This code is a Serial in parallel out shift register
//It takes in an 8 bit int and shifts it on each rising edge of enable
//////////////////////////////////////////////////////////////////////////////////
module SIPO1to8(
    input [15:0] Din,
    input En,
    output reg [15:0] out0 =16'b1111111111111111,
    output reg [15:0] out1 =16'b1111111111111111,
    output reg [15:0] out2 =16'b1111111111111111,
    output reg [15:0] out3 =16'b1111111111111111,
    output reg [15:0] out4 =16'b1111111111111111,
    output reg [15:0] out5 =16'b1111111111111111,
    output reg [15:0] out6 =16'b1111111111111111,
    output reg [15:0] out7 =16'b1111111111111111
    );
	
	
//shifts on clock edge
always @(posedge En)
begin
	out7 <= out6;
	out6 <= out5;
	out5 <= out4;
	out4 <= out3;
	out3 <= out2;
	out2 <= out1;
	out1 <= out0;
	out0 <= Din;
end

endmodule
