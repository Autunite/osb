`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Bushnelllabs
// Engineer: George Bushnell
// 
// Create Date:    6/26/2019 
// Design Name: 
// Module Name:     
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
///////////////////////////////////////////////////////////////////////////////////

module Sandbox
(

//inputs
input clk,
input reset,

//outputs
output [7:0] ledBank

);

clk8_div div1
(
	.clk_in(clk),
	.reset(reset),
    .clk_out(ledBank[0])
);

endmodule