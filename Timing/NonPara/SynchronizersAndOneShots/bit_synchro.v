`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    16:50:24 06/20/2018 
// Design Name: 
// Module Name:    bit_synchro 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: This is a flag synchronizer that I developed because the one's
//		I found on the internet sucked and couldn't handle my design requirements.
//		Theoretically this should be able to handle any variation of clocks between
//		clkB and clkA, though they shouldn't be very close to each other in frequencies.
//		This above specification is probably violated because the FIFO clock can be both 
//		slower and faster than the TX clock and will probably pass close to the RX clock 
//		in frequency. Also this design needs to work without a reset signal (at least not
//		one used in the actual implementation, though one may be needed to make it work in
//		simulation). This is because the reset signals are sent by some of these 
//		synchronizers, if this is impossible then rework will be needed in all modules that
//		have a reset signal so that they send out an acknowledge signal when they reset so that
//		the FSM controlling resets makes sure that all module are in a reset state before 
//		de-asserting the reset signal. Wish me luck reader.
//
//		IMPORTANT: The synchro will take 1.5-2 clock cycles of the clkB to fully reset. 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: It works! Both ways! Faster to slower and slower to faster
//		clock domains. Aww yiss.
//
//		6/21/2018: Well it worked in simulation and the syntax is correct, but in 
//			an attempt to create an RTL schematic it complained about reg D being
//			connected to multiple drivers, so I must look at it and see how I can
//			Disconnect the connection driving D while the output is reading it and
//			resetting it. Whelp time for lunch.
//
//			Update: I fixed the problem but discovered a new one. If multiple pulses
//			are sent from a fast clock to a slower one within the time the ack flag
//			takes to be reset then the later pulses will be ignored. This may be fine
//			but if in the design it proves to be a problem then a busy signal will 
//			need to be linked out and the corresponding modules will need to be 
//			refactored.
//
//////////////////////////////////////////////////////////////////////////////////
module bit_synchro(
    input clkA,
    input flagA,
    input clkB,
	input reset,
    output reg flagOut_B
    );
	 
	 reg A;
	 reg B;
	 reg C;
	 reg D;
	 reg E;
	 
	 //figuring out how to make all the output values known to the
	 //simulator without using a reset signal
	 /*
	 initial begin
		 flagOut_B 		<= 0;
		 A				<= 0;
		 B				<= 0;
		 C				<= 0;
		 D				<= 0;
		 E				<= 0;
	 end
	 */
	 
	 //clkA logic
	 //input logic
	 always@(posedge clkA)//, posedge reset)
	 begin
		 
		 if(reset)
		 begin
			 A					<= flagA;
			 B					<= 0;
			 C					<= 0;
			 D					<= 0;
		 end
		 
		 else
		 begin
			A					<= flagA;
			B					<= A;
			C					<= B;
			//detect a rising edge
			if(C==0 && B ==1)
			begin
				D 				<= 1;
			end
		
			//unset D when ack flag is detected
			else if(E==1)
			begin
				D				<= 0;
			end
		  end
	 end
	 
	 //clkB logic
	 //output logic
	 always@(posedge clkB)//, posedge reset)
	 begin
		 
		if(reset)
		begin
			E				<= 0;
			flagOut_B		<= 0;
		end
		
		else
		begin
			//if D == 1 and ack flag is 0 then put out a pulse
			//one clkB period long, and set ack flag
			if(D == 1 && E==0)
			begin
				//D				<= 0;
				E				<= 1;
				flagOut_B		<= 1;
			end
		
			//if ack flag is set and D == 0
			//reset ack flag
			else if(E== 1 && D ==0)
			begin
				E				<= 0;
			end
		
			//terminates flag B pulse
			//theoretically
			//must do extensive testing
			//incase they lock up
			if(flagOut_B == 1)
			begin
				flagOut_B		<= 0;
			end
		end
	 end


endmodule
