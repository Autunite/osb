`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:03:26 06/20/2018
// Design Name:   bit_synchro
// Module Name:   C:/Users/George/Documents/work/EPCPOWER/Uartchallenge/bitsynchro_tb.v
// Project Name:  Uartchallenge
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: bit_synchro
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module bitsynchro_tb;

	// Inputs
	reg clkA;
	reg flagA;
	reg clkB;

	// Outputs
	wire flagOut_B;

	// Instantiate the Unit Under Test (UUT)
	bit_synchro uut (
		.clkA(clkA), 
		.flagA(flagA), 
		.clkB(clkB), 
		.flagOut_B(flagOut_B)
	);

	initial begin
		// Initialize Inputs
		clkA = 0;
		flagA = 0;
		clkB = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		flagA = 1;
		#2.5;
		flagA = 0;
		#2.5;
		flagA = 1;
		#2.5;
		flagA = 0;
		

	end
	
	//clkA
	always #1.25  clkA =  ! clkA;
	
	//clkB
	always #52.63  clkB =  ! clkB;
      
endmodule

