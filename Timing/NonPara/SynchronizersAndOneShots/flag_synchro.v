`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:50:40 06/18/2018 
// Design Name: 
// Module Name:    flag_synchro 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module flag_synchro(
    input clkA,
    input flagA,
    input clkB,
	 input reset,
    output reg flagout_clkB
    );

reg FlagToggle_clkA 	= 0;
reg [2:0] SyncA_clkB = 0;

/*
initial begin
	FlagToggle_clkA	=0;
	SyncA_clkB			=0;
	flagout_clkB		=0;
end
*/

always @(posedge clkA) 
begin
	if(reset == 1)
	begin
		FlagToggle_clkA	<=0;
		SyncA_clkB			<=0;
		flagout_clkB		<=0;
	end
	
	else
	begin
	FlagToggle_clkA <= FlagToggle_clkA ^ flagA;  // when flag is asserted, this signal toggles (clkA domain)
	end
	
end



always @(posedge clkB) 
begin

	if(reset == 1)
	begin
		FlagToggle_clkA	<=0;
		SyncA_clkB			<=0;
		flagout_clkB		<=0;
	end

	else
	begin
	SyncA_clkB <= {SyncA_clkB[1:0], FlagToggle_clkA};  // now we cross the clock domains
	end
	
end

assign flagOut_clkB = (SyncA_clkB[2] ^ SyncA_clkB[1]);  // and create the clkB flag


endmodule
