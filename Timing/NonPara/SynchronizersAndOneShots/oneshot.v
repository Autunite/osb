`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    09:46:10 05/29/2018 
// Design Name: 
// Module Name:    oneshot 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: creates a one shot by detecting a rising edge on the enable line
//	and puts out a pulse a clock period or two wide, will test for metastability issues
// The enable line must be held high for at least one entire clock period for the one
// shot to register the rising edge.
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module oneshot(
    input enable,
    input clk,
	input reset,
    output oneshot
    );

	reg a, b, c;

/*
initial begin
	a <=0;
	b <=0;
	c <=0;
end
*/

always@(posedge clk)
begin
	
	if(reset)
	begin
		a <=0;
		b <=0;
		c <=0;
	end
	
	else
	begin
		a <= enable;
		b <= a;
		c <= b;
	end
	
end

assign oneshot = !c && b; //|| !b && a; 

endmodule
