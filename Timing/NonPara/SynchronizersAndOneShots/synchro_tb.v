`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:59:48 06/20/2018
// Design Name:   flag_synchro
// Module Name:   C:/Users/George/Documents/work/EPCPOWER/Uartchallenge/synchro_tb.v
// Project Name:  Uartchallenge
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: flag_synchro
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module synchro_tb;

	// Inputs
	reg clkA;
	reg flagA;
	reg clkB;
	reg reset;

	// Outputs
	wire flagout_clkB;

	// Instantiate the Unit Under Test (UUT)
	flag_synchro uut (
		.clkA(clkA), 
		.flagA(flagA), 
		.clkB(clkB), 
		.reset(reset),
		.flagout_clkB(flagout_clkB)
	);

	initial begin
		// Initialize Inputs
		clkA = 0;
		flagA = 0;
		clkB = 0;

		// Wait 100 ns for global reset to finish
		#100;
      reset = 1;
		#10;
		reset = 0;
		#10;
		  
		// Add stimulus here
		flagA = 1;
		#10;
		flagA = 0;
		

	end
	
	//clkA
	always #1.25  clkA =  ! clkA;
	
	//clkB
	always #52.63  clkB =  ! clkB;
      
endmodule

