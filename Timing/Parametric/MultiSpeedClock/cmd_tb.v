`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:54:34 06/06/2018
// Design Name:   command_module
// Module Name:   C:/Users/George/Documents/work/EPCPOWER/Uartchallenge/cmd_tb.v
// Project Name:  Uartchallenge
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: command_module
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module cmd_tb;

	// Inputs
	reg [7:0] charIn;
	reg load;
	reg clk_in;
	reg reset;

	// Outputs
	wire [7:0] clkspd;
	wire [1:0] seq_len;
	wire sample_state;
	//wire start_sam;
	
	integer i = 0;

	// Instantiate the Unit Under Test (UUT)
	command_module uut (
		.charIn(charIn), 
		.load(load), 
		.clk_in(clk_in), 
		.reset(reset), 
		.clkspd(clkspd), 
		.seq_len(seq_len), 
		.sample_state(sample_state) 
		//.start_sam(start_sam)
	);

	initial begin
		// Initialize Inputs
		charIn = 0;
		load = 0;
		clk_in = 0;
		reset = 0;

		// Wait 100 ns for global reset to finish
		#30;
		reset = 1;
		#10;
		reset = 0;
		#10;
		
		// run through all characters 0-255
		for(i = 0; i<255; i=i+1)
		begin
		charIn = "!";
		load = 1;
		#10;
		charIn = "!";
		load = 0;
		#10;
		charIn = i;
		load = 1;
		#10;
		load = 0;
		#10;
		end
		
        
		// Add stimulus here

	end
	
	always 
       #5  clk_in =  ! clk_in;
      
endmodule

